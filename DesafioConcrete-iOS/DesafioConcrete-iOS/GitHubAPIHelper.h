//
//  GitHubAPIHelper.h
//  DesafioConcrete-iOS
//
//  Created by Leonardo Sousa on 18/06/17.
//  Copyright © 2017 Leonardo Sousa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

typedef void (^getDefaultBlock) (id returnObj, NSError* error);
typedef void (^getItemBlock) (NSDictionary* returnDictionary, NSError* error);
typedef void (^getListBlock) (NSArray* returnArray, NSError* error);

@interface GitHubAPIHelper : NSObject

+ (void) listRepositoriesWithPage: (int) pageToLoad withCompletion: (getItemBlock) completion;

+ (void) getPullRequestsFromRepositoryURL:(NSURL*) pullReuqestURL withCompletion: (getListBlock) completion;

+ (void) getDetailFromUser: (NSString*) userName withCompletion:(getItemBlock) completion;
@end
