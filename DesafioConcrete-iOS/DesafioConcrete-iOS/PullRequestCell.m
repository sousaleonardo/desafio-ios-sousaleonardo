//
//  PullRequestCell.m
//  DesafioConcrete-iOS
//
//  Created by Leonardo Sousa on 18/06/17.
//  Copyright © 2017 Leonardo Sousa. All rights reserved.
//

#import "PullRequestCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation PullRequestCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self.userPhotoImg.layer setCornerRadius: CGRectGetWidth(self.userPhotoImg.frame)/2];
}

- (void) prepareForReuse
{
    [self.titleLbl setText: @""];
    [self.bodyLbl setText: @""];
    
    [self.userPhotoImg setImage: [UIImage imageNamed: @""]];
    [self.usernameLbl setText: @""];
    [self.userFullNameLbl setText: @""];
}

- (void) setPullRequest: (PullRequest*) pullRequest
{
    [self.titleLbl setText: pullRequest.title];
    [self.bodyLbl setText: pullRequest.body];
    
    [self.usernameLbl setText: pullRequest.author.userName];
    [self.userFullNameLbl setText: pullRequest.author.fullName];
    [self.userPhotoImg sd_setImageWithURL: pullRequest.author.avatarURL placeholderImage: [UIImage imageNamed: @"defaultUserPhoto"] options: SDWebImageCacheMemoryOnly];
}
@end
