//
//  UserController.m
//  DesafioConcrete-iOS
//
//  Created by Leonardo Sousa on 19/06/17.
//  Copyright © 2017 Leonardo Sousa. All rights reserved.
//

#import "UserController.h"
#import "GitHubAPIHelper.h"

@implementation UserController

+ (void) getDetailForUser:(User*) user
{
    [GitHubAPIHelper getDetailFromUser: user.userName withCompletion:^(NSDictionary *returnDictionary, NSError *error) {
        
        __block User* userRef;
        if (!error)
        {
            NSError* parseError;
            User* returnedUser = [MTLJSONAdapter modelOfClass: [User class] fromJSONDictionary: returnDictionary error: &parseError];
            
            if (!parseError)
            {
                userRef = returnedUser;
            }
        }
    }];
}
@end
