//
//  PullRequestListViewController.h
//  DesafioConcrete-iOS
//
//  Created by Leonardo Sousa on 18/06/17.
//  Copyright © 2017 Leonardo Sousa. All rights reserved.
//

#import "BaseViewController.h"
#import "Repository.h"

@interface PullRequestListViewController : BaseViewController
@property Repository* currentRepository;
@end
