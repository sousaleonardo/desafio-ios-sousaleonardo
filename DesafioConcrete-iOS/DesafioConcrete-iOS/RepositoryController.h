//
//  RepositoryController.h
//  DesafioConcrete-iOS
//
//  Created by Leonardo Sousa on 18/06/17.
//  Copyright © 2017 Leonardo Sousa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>

#import "Repository.h"

@protocol RepositoryControllerDelegate <NSObject>

@optional
- (void) getRepositoriesSucessful;
- (void) getRepositoriesError: (NSError*) error;

- (void) getPullRequestSucessful;
- (void) getPullRequestError: (NSError*) error;
@end
@interface RepositoryController : NSObject

@property int nextRepoPage;
@property NSMutableArray <Repository*>* repositories;
@property id <RepositoryControllerDelegate> delegate;

+ (RepositoryController*) sharedRepositoryController;

- (void) getRepositoriesList;
- (void) getNextRepositoriesList;

- (void) getPullRequestsForRepository: (Repository*) repository;
- (NSNumber*) openedPullRequestForRepository: (Repository*) repository;
- (NSNumber*) closedPullRequestForRepository: (Repository*) repository;
@end
