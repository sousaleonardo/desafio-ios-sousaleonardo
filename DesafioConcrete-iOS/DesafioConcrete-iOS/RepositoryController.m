//
//  RepositoryController.m
//  DesafioConcrete-iOS
//
//  Created by Leonardo Sousa on 18/06/17.
//  Copyright © 2017 Leonardo Sousa. All rights reserved.
//

#import "RepositoryController.h"
#import "GitHubAPIHelper.h"

@implementation RepositoryController

- (id) init
{
    if (self == [super init])
    {
        self.repositories = [NSMutableArray new];
        self.nextRepoPage = 1;
    }
    
    return self;
}

+ (RepositoryController*) sharedRepositoryController
{
    static dispatch_once_t pred;
    static RepositoryController* shared = nil;
    
    dispatch_once(&pred, ^{
        shared = [[RepositoryController alloc] init];
    });
    
    return shared;
}

- (void) getNextRepositoriesList
{
    [GitHubAPIHelper listRepositoriesWithPage: self.nextRepoPage withCompletion: ^(NSDictionary *returnDictionary, NSError *error)
     {
         if (!error)
         {
             NSError* parseError;
             NSArray* returnedRepositories = [MTLJSONAdapter modelsOfClass: [Repository class]
                                                             fromJSONArray: [returnDictionary objectForKey: @"items"]
                                                                     error: &parseError];
             
             if (!parseError)
             {
                 [self.repositories addObjectsFromArray: returnedRepositories];
                 self.nextRepoPage ++;
                 
                 if ([self.delegate respondsToSelector: @selector(getRepositoriesSucessful)])
                 {
                     [self.delegate getRepositoriesSucessful];
                 }
             }
             else
             {
                 if ([self.delegate respondsToSelector: @selector(getRepositoriesError:)])
                 {
                     [self.delegate getRepositoriesError: parseError];
                 }
             }
         }
         else
         {
             if ([self.delegate respondsToSelector: @selector(getRepositoriesError:)])
             {
                 [self.delegate getRepositoriesError: error];
             }
         }
     }];
}

- (void) getRepositoriesList
{
    self.nextRepoPage = 1;
    
    [GitHubAPIHelper listRepositoriesWithPage: self.nextRepoPage withCompletion: ^(NSDictionary *returnDictionary, NSError *error)
     {
         if (!error)
         {
             NSError* parseError;
             NSArray* returnedRepositories = [MTLJSONAdapter modelsOfClass: [Repository class]
                                                             fromJSONArray: [returnDictionary objectForKey: @"items"]
                                                                     error: &parseError];
             
             if (!parseError)
             {
                 self.repositories = [NSMutableArray arrayWithArray: returnedRepositories];
                 self.nextRepoPage ++;
                 
                 if ([self.delegate respondsToSelector: @selector(getRepositoriesSucessful)])
                 {
                     [self.delegate getRepositoriesSucessful];
                 }
             }
             else
             {
                 if ([self.delegate respondsToSelector: @selector(getRepositoriesError:)])
                 {
                     [self.delegate getRepositoriesError: parseError];
                 }
             }
         }
         else
         {
             if ([self.delegate respondsToSelector: @selector(getRepositoriesError:)])
             {
                 [self.delegate getRepositoriesError: error];
             }
         }
     }];
}

- (void) getPullRequestsForRepository: (Repository*) repository
{
    [GitHubAPIHelper getPullRequestsFromRepositoryURL: repository.pullRequestsURL withCompletion: ^(NSArray *returnArray, NSError *error) {
        
        if (!error)
        {
            NSError* parseError;
            NSArray* returnedPullRequests = [MTLJSONAdapter modelsOfClass: [PullRequest class]
                                                            fromJSONArray: returnArray
                                                                    error: &parseError];
            
            if (!parseError)
            {
                [repository setPullRequests: returnedPullRequests];
                
                if ([self.delegate respondsToSelector: @selector(getPullRequestSucessful)])
                {
                    [self.delegate getPullRequestSucessful];
                }
            }
            else
            {
                if ([self.delegate respondsToSelector: @selector(getPullRequestError:)])
                {
                    [self.delegate getRepositoriesError: parseError];
                }
            }
        }
        else
        {
            if ([self.delegate respondsToSelector: @selector(getPullRequestError:)])
            {
                [self.delegate getRepositoriesError: error];
            }
        }
    }];
}

- (NSNumber*) openedPullRequestForRepository: (Repository*) repository
{
    NSArray* openedPullRequests = [self filterRepositoryPullRequest: repository.pullRequests forState: PullRequestStateOpen];
    
    return  [NSNumber numberWithInteger: openedPullRequests.count];
}

- (NSNumber*) closedPullRequestForRepository: (Repository*) repository
{
    NSArray* closedPullRequests = [self filterRepositoryPullRequest: repository.pullRequests forState: PullRequestStateClosed];
    
    return [NSNumber numberWithInteger: closedPullRequests.count];
}

- (NSArray*) filterRepositoryPullRequest: (NSArray*) pullRequests forState:(PullRequestState) state
{
    NSPredicate* predicate = [NSPredicate predicateWithFormat: @"state == %d", state];
    
    return [pullRequests filteredArrayUsingPredicate: predicate];
}
@end
