//
//  UserController.h
//  DesafioConcrete-iOS
//
//  Created by Leonardo Sousa on 19/06/17.
//  Copyright © 2017 Leonardo Sousa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>

#import "User.h"

@interface UserController : NSObject

+ (void) getDetailForUser:(User*) user;
@end
