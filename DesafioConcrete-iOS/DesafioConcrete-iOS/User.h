//
//  User.h
//  DesafioConcrete-iOS
//
//  Created by Leonardo Sousa on 18/06/17.
//  Copyright © 2017 Leonardo Sousa. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface User : MTLModel <MTLJSONSerializing>

@property NSNumber* idUser;

@property NSString* userName;
@property NSString* fullName;
@property NSURL*    avatarURL;

@end
