//
//  PullRequest.h
//  DesafioConcrete-iOS
//
//  Created by Leonardo Sousa on 18/06/17.
//  Copyright © 2017 Leonardo Sousa. All rights reserved.
//

#import <Mantle/Mantle.h>
#import "User.h"

typedef enum : NSUInteger {
    PullRequestStateOpen,
    PullRequestStateClosed
} PullRequestState;

@interface PullRequest : MTLModel <MTLJSONSerializing>

@property NSNumber* idPullRequest;

@property NSString* title;
@property NSString* body;
@property NSURL*    url;
@property User* author;
@property PullRequestState state;

@end
