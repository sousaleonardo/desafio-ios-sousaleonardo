//
//  RepositoryListViewController.m
//  DesafioConcrete-iOS
//
//  Created by Leonardo Sousa on 18/06/17.
//  Copyright © 2017 Leonardo Sousa. All rights reserved.
//

#import "RepositoryListViewController.h"
#import "PullRequestListViewController.h"
#import "UserController.h"
#import "RepositoryCell.h"

@interface RepositoryListViewController ()
@end

@implementation RepositoryListViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    self.controller = [RepositoryController sharedRepositoryController];
    [self.controller setDelegate: self];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    
    if (self.controller.repositories.count == 0)
    {
        [self requestNewValues: nil];
    }
}
- (void) requestNewValues: (id)sender
{
    if (!sender)
    {
        [self presentLoader];
        [self.controller getNextRepositoriesList];
    }
    else
    {
        [self.refreshControl beginRefreshing];
        [self.controller getRepositoriesList];
    }
}

#pragma mark TableView Delegate
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.controller.repositories count];
}

- (RepositoryCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    RepositoryCell* cell = [tableView dequeueReusableCellWithIdentifier: @"repositoryCell" forIndexPath: indexPath];
    
    Repository* repository = [self.controller.repositories objectAtIndex: indexPath.row];
    [cell setRepository: repository];
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath: indexPath animated: YES];
    
    [self performSegueWithIdentifier: @"repositoryPullRequestSegue" sender: [self.controller.repositories objectAtIndex: indexPath.row]];
}

- (void) tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    
    if (indexPath.row == self.controller.repositories.count - 15)
    {
        [self requestNewValues: nil];
    }
    
    [cell setAlpha: 0];
    
    [UIView animateWithDuration: 0.5 animations:^{
        [cell setAlpha: 1];
    }];
}
#pragma mark Navigation
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender: (id)sender
{
    if ([segue.destinationViewController isKindOfClass: [PullRequestListViewController class]])
    {
        PullRequestListViewController* nextViewController = (PullRequestListViewController*) segue.destinationViewController;
        [nextViewController setCurrentRepository: sender];
    }
}
#pragma mark RepositoryController Delegate
- (void) getRepositoriesSucessful
{
    [self.refreshControl endRefreshing];
    [self removeLoader];
    [self updateValues];
}

- (void) getRepositoriesError:(NSError *)error
{
    [self removeLoader];
    [self presentAlertWithTitle: NSLocalizedString(@"Error", NO) message: [error localizedDescription]];
}
@end
