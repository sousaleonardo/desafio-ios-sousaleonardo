//
//  Repository.m
//  DesafioConcrete-iOS
//
//  Created by Leonardo Sousa on 18/06/17.
//  Copyright © 2017 Leonardo Sousa. All rights reserved.
//

#import "Repository.h"
#import "NSStringToPullRequesURLTransformer.h"

@implementation Repository

+ (NSDictionary*) JSONKeyPathsByPropertyKey
{
    return @{
             @"idRepository"    : @"id",
             @"name"            : @"name",
             @"descRepository"  : @"description",
             @"forks" : @"forks_count",
             @"stars" : @"stargazers_count",
             @"owner" : @"owner",
             @"pullRequestsURL" : @"pulls_url"
             };
}

+ (NSValueTransformer *)JSONTransformerForKey:(NSString *)key
{
    if ([key isEqualToString: @"pullRequestsURL"])
    {
        return [NSStringToPullRequesURLTransformer new];
    }
    
    return nil;
}
@end
