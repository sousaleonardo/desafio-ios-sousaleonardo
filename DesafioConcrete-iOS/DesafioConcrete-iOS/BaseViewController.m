//
//  BaseViewController.m
//  DesafioConcrete-iOS
//
//  Created by Leonardo Sousa on 18/06/17.
//  Copyright © 2017 Leonardo Sousa. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@property ARSLineProgress* activityIndicator;

@end

@implementation BaseViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Setup default navigationColor and icon to back button
    [self.navigationController.navigationBar setTintColor: [UIColor whiteColor]];
    [self.navigationController.navigationBar setBarTintColor: [UIColor blackColor]];
    [self.navigationController.navigationBar setTitleTextAttributes: [NSDictionary dictionaryWithObject: [UIColor whiteColor] forKey: NSForegroundColorAttributeName]];
    
    [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor clearColor]} forState:UIControlStateNormal];
    [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor clearColor]} forState:UIControlStateHighlighted];
    
    //Insert pull to refresh item
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget: self action: @selector(requestNewValues:) forControlEvents: UIControlEventValueChanged];
    [self.tableView addSubview: self.refreshControl];
    
    //RepositoryController
    self.controller = [RepositoryController sharedRepositoryController];
    [self.controller setDelegate: self];
    
    [self.tableView setRowHeight: UITableViewAutomaticDimension];
    [self.tableView setEstimatedRowHeight: 130];
}

- (void) updateValues
{
    [self.tableView reloadData];
}

- (void) presentLoader
{
    [ARSLineProgress ars_showOnView: self.tableView];
}

- (void) removeLoader
{
    [ARSLineProgress hide];
}

- (void) presentAlertWithTitle: (NSString*) title message: (NSString*) message
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle: title message: message preferredStyle: UIAlertControllerStyleAlert];
    
    [alert addAction: [UIAlertAction actionWithTitle: NSLocalizedString(@"OK", NO)
                                               style: UIAlertActionStyleDefault
                                             handler: ^(UIAlertAction * _Nonnull action)
    {
        
        [alert dismissViewControllerAnimated: YES completion: nil];
    }]];
    
    [self presentViewController: alert animated: YES completion: nil];
}
@end
