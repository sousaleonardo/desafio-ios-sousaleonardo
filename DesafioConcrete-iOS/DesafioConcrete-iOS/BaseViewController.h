//
//  BaseViewController.h
//  DesafioConcrete-iOS
//
//  Created by Leonardo Sousa on 18/06/17.
//  Copyright © 2017 Leonardo Sousa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ARSLineProgress/ARSLineProgress-Swift.h>
#import "RepositoryController.h"

@interface BaseViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource, RepositoryControllerDelegate>

@property RepositoryController* controller;

- (void) requestNewValues: (id) sender;
- (void) updateValues;
- (void) presentLoader;
- (void) removeLoader;
- (void) presentAlertWithTitle: (NSString*) title message: (NSString*) message;
@end
