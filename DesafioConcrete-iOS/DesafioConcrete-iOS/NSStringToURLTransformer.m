//
//  NSStringToURLTransformer.m
//  DesafioConcrete-iOS
//
//  Created by Leonardo Sousa on 18/06/17.
//  Copyright © 2017 Leonardo Sousa. All rights reserved.
//

#import "NSStringToURLTransformer.h"

@implementation NSStringToURLTransformer

+ (Class) transformedValueClass
{
    return [NSURL class];
}

+ (BOOL) allowsReverseTransformation
{
    return YES;
}

- (id) transformedValue: (id)value
{
    if (value && [value isKindOfClass: [NSString class]])
    {
        return [NSURL URLWithString: value];
    }
    
    return nil;
}

- (id) reverseTransformedValue:(id)value
{
    if (value && [value isKindOfClass: [NSURL class]])
    {
        return [value absoluteString];
    }
    
    return nil;
}

@end
