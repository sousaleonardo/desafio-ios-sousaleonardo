//
//  GitHubAPIHelper.m
//  DesafioConcrete-iOS
//
//  Created by Leonardo Sousa on 18/06/17.
//  Copyright © 2017 Leonardo Sousa. All rights reserved.
//

#import "GitHubAPIHelper.h"

#define BASEURL @"https://api.github.com/"
#define REPOSEARCH @"search/repositories"
#define USERSEARCH @"users/"
@implementation GitHubAPIHelper

+ (NSString*) mountRequestString:(NSString*) request
{
    return [NSString stringWithFormat:@"%@%@", BASEURL, request];
}

+ (void) defaultGETRequestWithURL: (NSString*) requestURL parameters:(NSDictionary*) parameters andCompletion:(getDefaultBlock) completion
{
    if ([[AFNetworkReachabilityManager sharedManager] networkReachabilityStatus] != AFNetworkReachabilityStatusNotReachable)
    {
        AFHTTPSessionManager* manager = [AFHTTPSessionManager manager];
        
        [manager GET: requestURL
          parameters: parameters
            progress: nil
             success: ^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                 
                 if (completion)
                 {
                     completion (responseObject, nil);
                 }
             }
             failure: ^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                 if (completion)
                 {
                     completion (nil, error);
                 }
             }];
    }
    else
    {
        NSError* error = [NSError errorWithDomain: @"GitHubAPI"
                                             code: 1
                                         userInfo: [NSDictionary dictionaryWithObject :  NSLocalizedString(@"noInternetError", NO)
                                                                               forKey : NSLocalizedDescriptionKey]];
        
        if (completion) {
            completion (nil, error);
        }
    }
}

+ (void) listRepositoriesWithPage: (int) pageToLoad withCompletion: (getItemBlock) completion
{
    NSDictionary* parameters = [NSDictionary dictionaryWithObjects: @[@"language:Java", @"stars", [NSNumber numberWithInt: pageToLoad]]
                                                           forKeys: @[@"q", @"sort", @"page"]];

    
    [GitHubAPIHelper defaultGETRequestWithURL: [GitHubAPIHelper mountRequestString:     REPOSEARCH]
                                   parameters: parameters
                                andCompletion: completion];
}

+ (void) getPullRequestsFromRepositoryURL:(NSURL*) pullRequestURL withCompletion: (getListBlock) completion
{
    NSDictionary* parameters = [NSDictionary dictionaryWithObject: @"all" forKey: @"state"];
    
    [GitHubAPIHelper defaultGETRequestWithURL: [pullRequestURL absoluteString]
                                   parameters: parameters
                                andCompletion: completion];
}

+ (void) getDetailFromUser: (NSString*) userName withCompletion:(getItemBlock) completion
{
    NSString* urlString = [NSString stringWithFormat: @"%@/%@",[GitHubAPIHelper mountRequestString: USERSEARCH], userName];
    
    [GitHubAPIHelper defaultGETRequestWithURL: urlString parameters: nil andCompletion: completion];
}
@end
