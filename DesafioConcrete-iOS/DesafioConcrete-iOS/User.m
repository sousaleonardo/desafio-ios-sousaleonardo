//
//  User.m
//  DesafioConcrete-iOS
//
//  Created by Leonardo Sousa on 18/06/17.
//  Copyright © 2017 Leonardo Sousa. All rights reserved.
//

#import "User.h"
#import "NSStringToURLTransformer.h"

@implementation User

+ (NSDictionary*) JSONKeyPathsByPropertyKey
{
    return @{
        @"idUser" : @"id",
        @"userName" : @"login",
        @"fullName" : @"name",
        @"avatarURL" : @"avatar_url"
    };
}

+ (NSValueTransformer *)JSONTransformerForKey:(NSString *)key
{
    if ([key isEqualToString: @"avatar_url"])
    {
        return [NSStringToURLTransformer new];
    }
    
    return nil;
}
@end
