//
//  Repository.h
//  DesafioConcrete-iOS
//
//  Created by Leonardo Sousa on 18/06/17.
//  Copyright © 2017 Leonardo Sousa. All rights reserved.
//

#import <Mantle/Mantle.h>
#import "User.h"
#import "PullRequest.h"

@interface Repository : MTLModel <MTLJSONSerializing>

@property NSNumber* idRepository;

@property NSString* name;
@property NSString* descRepository;
@property NSNumber* forks;
@property NSNumber* stars;
@property NSURL*    pullRequestsURL;

@property NSNumber* openedPullRequests;
@property NSNumber* closedPullRequests;

@property User*     owner;
@property NSArray <PullRequest *>* pullRequests;

@end
