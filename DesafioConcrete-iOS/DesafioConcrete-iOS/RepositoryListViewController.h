//
//  RepositoryListViewController.h
//  DesafioConcrete-iOS
//
//  Created by Leonardo Sousa on 18/06/17.
//  Copyright © 2017 Leonardo Sousa. All rights reserved.
//

#import "BaseViewController.h"


@interface RepositoryListViewController : BaseViewController <RepositoryControllerDelegate>

@end
