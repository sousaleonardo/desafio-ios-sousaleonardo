//
//  PullRequestListViewController.m
//  DesafioConcrete-iOS
//
//  Created by Leonardo Sousa on 18/06/17.
//  Copyright © 2017 Leonardo Sousa. All rights reserved.
//

#import "PullRequestListViewController.h"
#import "PullRequestCell.h"

@interface PullRequestListViewController ()

@end

@implementation PullRequestListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self requestNewValues: nil];
    
    [self setTitle: self.currentRepository.name];
}

- (void) requestNewValues:(id)sender
{
    if (self.currentRepository)
    {
        if (!sender) {
            [self presentLoader];
        }
        
        [self.controller getPullRequestsForRepository: self.currentRepository];
    }
}
#pragma mark RepositoryController Delegate
- (void) getPullRequestSucessful
{
    //Update Repository info
    self.currentRepository.openedPullRequests = [self.controller openedPullRequestForRepository: self.currentRepository];
    self.currentRepository.closedPullRequests = [self.controller closedPullRequestForRepository: self.currentRepository];
    
    [self updateValues];
    [self removeLoader];
    [self.refreshControl endRefreshing];
}

- (void) getPullRequestError:(NSError *)error
{
    [self removeLoader];
    [self presentAlertWithTitle: NSLocalizedString(@"Error", NO) message: [error localizedDescription]];
}

#pragma mark TableView Delegate
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!self.currentRepository.pullRequests)
    {
        return 0;
    }
    
    return self.currentRepository.pullRequests.count;
}

- (PullRequestCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PullRequestCell* pullRequestCell = [tableView dequeueReusableCellWithIdentifier: @"pullRequestCell" forIndexPath: indexPath];
    
    [pullRequestCell setPullRequest: [self.currentRepository.pullRequests objectAtIndex: indexPath.row]];
    
    return pullRequestCell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSURL* pullRequestPageURL = [(PullRequest*) [self.currentRepository.pullRequests objectAtIndex: indexPath.row] url];
    
    [[UIApplication sharedApplication] openURL: pullRequestPageURL];
    
    [tableView deselectRowAtIndexPath: indexPath animated: YES];
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (self.currentRepository.openedPullRequests && self.currentRepository.closedPullRequests)
    {
        return 40.0;
    }
    return 0;
}
- (UIView*) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView* headerView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, CGRectGetWidth(self.tableView.frame), 40)];
    
    [headerView setBackgroundColor: [UIColor whiteColor]];
    
    CGRect labelFrame = CGRectMake(0, 0, 100, 40);
    NSInteger fontSize = 13;
    
    UILabel* openedLbl = [[UILabel alloc] initWithFrame: labelFrame];
    [openedLbl setTextColor: [UIColor colorWithRed: 223/255.0 green: 145/255.0 blue: 0/255.0 alpha: 1]];
    [openedLbl setFont: [UIFont boldSystemFontOfSize: fontSize]];
    [openedLbl setText: [NSString stringWithFormat: @"%@ %@", [self.currentRepository.openedPullRequests stringValue], NSLocalizedString(@"opened", NO)]];
    [openedLbl sizeToFit];
    
    UILabel* separatorLbl = [[UILabel alloc] initWithFrame: labelFrame];
    [separatorLbl setFont: [UIFont boldSystemFontOfSize: fontSize]];
    [separatorLbl setTextColor: [UIColor blackColor]];
    [separatorLbl setText: @" / "];
    [separatorLbl sizeToFit];
    
    UILabel* closedLbl = [[UILabel alloc] initWithFrame: labelFrame];
    [closedLbl setFont: [UIFont boldSystemFontOfSize: fontSize]];
    [closedLbl setTextColor: [UIColor blackColor]];
    [closedLbl setText: [NSString stringWithFormat: @"%@ %@", [self.currentRepository.closedPullRequests stringValue], NSLocalizedString(@"closed", NO)]];
    [closedLbl sizeToFit];
    
    [separatorLbl setCenter: headerView.center];
    [headerView addSubview: separatorLbl];
    
    [openedLbl setCenter: CGPointMake(CGRectGetMinX(separatorLbl.frame) - CGRectGetMidX(openedLbl.frame), CGRectGetMidY(separatorLbl.frame))];
    [headerView addSubview: openedLbl];
    
    [closedLbl setCenter: CGPointMake(CGRectGetMaxX(separatorLbl.frame) + CGRectGetMidX(closedLbl.frame), CGRectGetMidY(separatorLbl.frame))];
    [headerView addSubview: closedLbl];
    
    return headerView;
    
}
@end
