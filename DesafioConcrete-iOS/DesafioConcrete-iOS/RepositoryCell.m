//
//  RepositoryCell.m
//  DesafioConcrete-iOS
//
//  Created by Leonardo Sousa on 18/06/17.
//  Copyright © 2017 Leonardo Sousa. All rights reserved.
//

#import "RepositoryCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface RepositoryCell ()

@property (weak, nonatomic) IBOutlet UIImageView *forksImg;
@property (weak, nonatomic) IBOutlet UIImageView *starsImg;

@end

@implementation RepositoryCell

- (void) awakeFromNib
{
    [super awakeFromNib];
    
    [self.starsImg setImage: [self.starsImg.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
    [self.forksImg setImage: [self.forksImg.image imageWithRenderingMode: UIImageRenderingModeAlwaysTemplate]];
    
    [self.userPhotoImg.layer setCornerRadius: CGRectGetWidth(self.userPhotoImg.frame)/2];
}
- (void) prepareForReuse
{
    [super prepareForReuse];
    
    [self.nameLbl setText: @""];
    [self.descriptionLbl setText: @""];
    [self.forksLbl setText: @""];
    [self.starsLbl setText: @""];
    
    [self.usernameLbl setText: @""];
    [self.userFullNameLbl setText: @""];
    [self.userPhotoImg setImage: [UIImage imageNamed: @"defaultUserPhoto"]];
}

- (void) setRepository: (Repository*) repository
{
    [self.nameLbl setText: repository.name];
    [self.descriptionLbl setText: repository.descRepository];
    [self.forksLbl setText: [repository.forks stringValue]];
    [self.starsLbl setText: [repository.stars stringValue]];
    
    [self.usernameLbl setText: repository.owner.userName];
    [self.userFullNameLbl setText: repository.owner.fullName];
    [self.userPhotoImg sd_setImageWithURL: repository.owner.avatarURL placeholderImage: [UIImage imageNamed: @"defaultUserPhoto"] options: SDWebImageCacheMemoryOnly];
}
@end
