//
//  PullRequest.m
//  DesafioConcrete-iOS
//
//  Created by Leonardo Sousa on 18/06/17.
//  Copyright © 2017 Leonardo Sousa. All rights reserved.
//

#import "PullRequest.h"
#import "NSStringToURLTransformer.h"

@implementation PullRequest

+ (NSDictionary*) JSONKeyPathsByPropertyKey
{
    return @{
             @"idPullRequest" : @"id",
             @"title" : @"title",
             @"body" : @"body",
             @"url" : @"html_url",
             @"author" : @"user",
             @"state" : @"state"
             };
}

+ (NSValueTransformer *)JSONTransformerForKey:(NSString *)key
{
    if ([key isEqualToString: @"url"])
    {
        return [NSStringToURLTransformer new];
    }
    
    return nil;
}

+ (NSValueTransformer *)stateJSONTransformer {
    return [NSValueTransformer mtl_valueMappingTransformerWithDictionary:@{
                                                                           @"open": @(PullRequestStateOpen),
                                                                           @"closed": @(PullRequestStateClosed)
                                                                           }];
}
@end
