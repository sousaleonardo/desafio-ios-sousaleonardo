//
//  NSStringToPullRequesURLTransformer.m
//  DesafioConcrete-iOS
//
//  Created by Leonardo Sousa on 19/06/17.
//  Copyright © 2017 Leonardo Sousa. All rights reserved.
//

#import "NSStringToPullRequesURLTransformer.h"

@implementation NSStringToPullRequesURLTransformer

- (id) transformedValue: (id)value
{
    if (value && [value isKindOfClass: [NSString class]])
    {
        NSCharacterSet* charSet = [NSCharacterSet characterSetWithCharactersInString: @"{"];
        NSArray* stringComponents = [value componentsSeparatedByCharactersInSet: charSet];
        return [NSURL URLWithString: [stringComponents objectAtIndex: 0]];
    }
    
    return nil;
}
@end
